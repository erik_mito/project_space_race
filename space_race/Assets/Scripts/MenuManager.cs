﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{

    public void Exit()
    {
        Debug.Log("Exit");
        Application.Quit();
    }
   /* public void Menu()
    {
        Debug.Log("Menu");
        SceneManager.LoadScene("MainMenu");
    }*/
   /* public void Credits()
    {
        Debug.Log("Credits");
        SceneManager.LoadScene("Credits");
    }*/
    public void Play()
    {
        Debug.Log("Play");
        SceneManager.LoadScene("Gameplay");
    }
	public void Menu()
    {
        Time.timeScale = 1; 
        Debug.Log("Menu");
        SceneManager.LoadScene("MainMenu");
    }
}