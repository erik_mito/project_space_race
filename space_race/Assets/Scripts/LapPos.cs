﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class LapPos : MonoBehaviour
{
    public LapTrack first;
	public Text textMesh;

	public int check =0;
	Vector3 position;
	LapTrack next;
	LapTrack last;
	
	public int _lap;
	float distance;

	// Use this for initialization
	void Start () {
		_lap = 0;
		SetNextTrigger(first);
		UpdateText();
	}

	// update lap counter text
	void UpdateText() {
		if (textMesh) {
			textMesh.text = string.Format("Lap {0}", _lap);		
		}
	}

	// when lap trigger is entered
	public void OnLapTrigger(LapTrack trigger) {
		if (trigger == next) {
			if (first == next) {
				_lap++;
				check = 0;
                //Debug.Log(_lap);
				UpdateText();
			}
			check++;
			last=trigger;
			SetNextTrigger(next);
		}
	}
		public float CheckDistance()
	{
		distance = Vector3.Distance (this.transform.position, next.transform.position);
		return distance;
	}		
	void SetNextTrigger(LapTrack trigger) {
		next = trigger.next;
		SendMessage("OnNextTrigger", next, SendMessageOptions.DontRequireReceiver);
	}

}
