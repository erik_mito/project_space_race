﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour
{
    [SerializeField] private GameObject pausePanel;
	[SerializeField] private GameObject final;
    public Text message;
	public int state = -1;
	public Text position;

	GameObject[] cars;

    void Start()
	{
		cars = GameObject.FindGameObjectsWithTag ("car");
	}	
   void OnEnable()
	{
        pausePanel.SetActive(true);
		Time.timeScale = 0f;
		StartCoroutine(Countdown(4));
	}		

	IEnumerator Countdown(int duration)
	{
		while (duration > 0)
		{
            switch (duration)
      {
          case 1:
              message.text = "GO";
              break;
          case 2:
              message.text = "Steady";
              break;
          case 3:
              message.text = "Ready";
              break;
      }
			duration --;
			yield return WaitForUnscaledSeconds(1f);
        }
        pausePanel.SetActive(false);
		Time.timeScale = 1f;
	}

	IEnumerator WaitForUnscaledSeconds(float dur)
	{
		var cur = 0f;
		while (cur < dur)
		{		
			yield return null;
			cur += Time.unscaledDeltaTime;
		}
	}
  
    void Update()
    {
		if(cars[1].GetComponent<LapPos>()._lap>cars[0].GetComponent<LapPos>()._lap)
		{
			position.text = "1st";
		}
		else if(cars[1].GetComponent<LapPos>()._lap==cars[0].GetComponent<LapPos>()._lap)
		{
			if(cars[1].GetComponent<LapPos>().check >cars[0].GetComponent<LapPos>().check)
			{
			position.text = "1st";
			}
			else if(cars[1].GetComponent<LapPos>().check==cars[0].GetComponent<LapPos>().check)
			{
				if(cars[1].GetComponent<LapPos>().CheckDistance()<cars[0].GetComponent<LapPos>().CheckDistance())
			{
			position.text = "1st";
			}
			else
			{
			position.text = "2nd";
			}
			}
			else
			{
			position.text = "2nd";
			}
		}
		else
		{
			position.text = "2nd";
		}

		if(cars[1].GetComponent<LapPos>()._lap==3 || cars[0].GetComponent<LapPos>()._lap==3)
		{
			final.SetActive(true);
			Time.timeScale = 0f;
		}
		
    }

}
