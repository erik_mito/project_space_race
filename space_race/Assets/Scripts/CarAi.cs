﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class CarAi : MonoBehaviour {
	public List<Transform> Waypoint;
	//Centro de gravedad del coche
	[SerializeField]
	[Range(0f, 1f)]
	float CGHeight = 0.55f;
	//Escala de inercia
	[SerializeField]
	[Range(0f, 2f)]
	float InertiaScale = 1f;

	[SerializeField]
	float BrakePower = 14000;

	//POTENCIA DEL FRENO
	[SerializeField]
	float EBrakePower = 5000;
	//Transferencia de masa del coche
	[SerializeField]
	[Range(0f, 1f)]
	float WeightTransfer = 0.35f;
	//Maximo angulo de giro
	[SerializeField]
	[Range(0f, 1f)]
	float MaxSteerAngle = 0.75f;

	[SerializeField]
	[Range(0f, 20f)]
	float CornerStiffnessFront = 5.0f;

	[SerializeField]
	[Range(0f, 20f)]
	float CornerStiffnessRear = 5.2f;
	//Resistencia que ejerce a la fuerza del aire
	[SerializeField]
	[Range(0f, 20f)]
	float AirResistance = 2.5f;

	[SerializeField]
	[Range(0f, 20f)]
	float RollingResistance = 8.0f;
	//Agarre freno delantero
	[SerializeField]
	[Range(0f, 1f)]
	float EBrakeGripRatioFront = 0.9f;
    //Potencia de agarre ruedas delanteras
	[SerializeField]
	[Range(0f, 5f)]
	float TotalTireGripFront = 2.5f;
	//Agarre freno delantero
	[SerializeField]
	[Range(0f, 1f)]
	float EBrakeGripRatioRear = 0.4f;
	//Potencia de agarre ruedas traseras
	[SerializeField]
	[Range(0f, 5f)]
	float TotalTireGripRear = 2.5f;
	//Velocidad de giro
	[SerializeField]
	//Velocidad a la que afecta el giro
	[Range(0f, 5f)]
	float SteerSpeed = 2.5f;
	public float turn = 5,lastTurn,Speed = 25;

	[SerializeField]
	[Range(0f, 5f)]
	float SteerAdjustSpeed = 1f;
	//Velocidad a la que se puede corregir la direccion/giro drifting
	[SerializeField]
	[Range(0f, 1000f)]
	float SpeedSteerCorrection = 300f;
	//Velocidad a la que se pierde el control del giro, cuanto mas alto mas control.
	[SerializeField]
	[Range(0f, 20f)]
	float SpeedTurningStability = 10f;

	[SerializeField]
	[Range(0f, 10f)]
	float AxleDistanceCorrection = 2f;

	[SerializeField]
	GameObject trailRight;
	
	[SerializeField]
	GameObject trailLeft;
	//devuelve la velocidad actual del coche
	public float SpeedKilometersPerHour {
		get {
			return Rigidbody2D.velocity.magnitude * 18f / 5f;
		}
	}

	// Variables that get initialized via code
	float Inertia = 1;
	float WheelBase = 1;
	float TrackWidth = 1;

	// Private vars
	float HeadingAngle;
	float AbsoluteVelocity;
	float AngularVelocity;
	float SteerDirection;
	float SteerAngle;
	int index;
	Vector3 oldEulerAngles;
	float test;

	Vector2 Velocity;
	Vector2 Acceleration;
	Vector2 LocalVelocity;
	Vector2 LocalAcceleration;
	Vector3 target;

	float Throttle;
	float Brake;
	float EBrake;

	float steerInput;
	
		Quaternion newRotation;

	Rigidbody2D Rigidbody2D;

	Axle AxleFront;
	Axle AxleRear;
	Engine Engine;

	GameObject CenterOfGravity;
	GameObject CameraView;

	AudioSource engineSound; 

	void Awake() {
		//asignamos "CameraView" al gameobject del coche
		CameraView = GameObject.Find ("CameraView").gameObject;

		Rigidbody2D = GetComponent<Rigidbody2D> ();
		CenterOfGravity = transform.Find ("CenterOfGravity").gameObject;

		AxleFront = transform.Find ("AxleFront").GetComponent<Axle>();
		AxleRear = transform.Find ("AxleRear").GetComponent<Axle>();

		Engine = transform.Find ("Engine").GetComponent<Engine>();

		engineSound = GetComponent<AudioSource> ();
		Init ();
	}

	void Init() {

		Velocity = Vector2.zero;
		AbsoluteVelocity = 0;

		// Dimensions
		AxleFront.DistanceToCG = Vector2.Distance(CenterOfGravity.transform.position, AxleFront.transform.Find("Axle").transform.position);
		AxleRear.DistanceToCG = Vector2.Distance(CenterOfGravity.transform.position, AxleRear.transform.Find("Axle").transform.position);
		// Extend the calculations past actual car dimensions for better simulation
		AxleFront.DistanceToCG *= AxleDistanceCorrection;
		AxleRear.DistanceToCG *= AxleDistanceCorrection;
			
		WheelBase = AxleFront.DistanceToCG + AxleRear.DistanceToCG;
		Inertia = Rigidbody2D.mass * InertiaScale;

		// Calculamos el angulo inicial del coche
		Rigidbody2D.rotation = transform.rotation.eulerAngles.z;
		HeadingAngle = (Rigidbody2D.rotation + 90) * Mathf.Deg2Rad;
	}

	void Start() {
		
		AxleFront.Init (Rigidbody2D, WheelBase);
		AxleRear.Init (Rigidbody2D, WheelBase);
		oldEulerAngles = transform.rotation.eulerAngles;
		TrackWidth = Vector2.Distance(AxleRear.TireLeft.transform.position, AxleRear.TireRight.transform.position);
		newRotation = Quaternion.LookRotation(transform.position - Waypoint[index].transform.position,Vector3.forward);
		newRotation.x = 0.0f;
        newRotation.y = 0.0f;
	}

	void Update() {
		// Calculate weight center of four tires
		// This is just to draw that red dot over the car to indicate what tires have the most weight
		Throttle=1;
		Vector2 pos = Vector2.zero;
		// Marca del derrape si se cumple la condición de la aceleracion o si esta pulsando el freno.
		if (Mathf.Abs (LocalAcceleration.y) > 18 || EBrake == 1) {
			AxleRear.TireRight.SetTrailActive (true);
			AxleRear.TireLeft.SetTrailActive (true);
			AxleRear.TireLeft.SetGasActive(true);
			AxleRear.TireRight.SetGasActive(true);
			AxleFront.TireLeft.SetGasActive(true);
			AxleFront.TireRight.SetGasActive(true);
		} else {
			AxleRear.TireRight.SetTrailActive (false);
			AxleRear.TireLeft.SetTrailActive (false);
			AxleRear.TireLeft.SetGasActive(false);
			AxleRear.TireRight.SetGasActive(false);
			AxleFront.TireLeft.SetGasActive(false);
			AxleFront.TireRight.SetGasActive(false);
		}

		// CONTROLAMOS LAS REVOLUCIONES POR MINUTO
		Engine.UpdateAutomaticTransmission (Rigidbody2D);

        // Si el coche es controlado por el usuario haremos que la camara le persiga poniendo la posicion del coche a CameraView
        CameraView.transform.position = this.transform.position;

		 var distance = Vector2.Distance(transform.position,Waypoint[index].transform.position);
    if(distance <= 2){
        index++;
    }
        if(index >= Waypoint.Count){
        index = 0;
    }	

		newRotation = Quaternion.LookRotation(transform.position - Waypoint[index].transform.position,Vector3.forward);			
        newRotation.x = 0.0f;
        newRotation.y = 0.0f;
        transform.rotation = Quaternion.Slerp(transform.rotation,newRotation, Time.deltaTime * turn);
		/*		if(turn<7f)
		{
    		lastTurn+=Time.deltaTime*Time.deltaTime*2f;
    		turn+=lastTurn;
		}  */
		steerInput=0;
		if(Mathf.Abs(oldEulerAngles.z - transform.rotation.eulerAngles.z)<0.1f)
		{
			steerInput=0;
      	} 
	  else{
		   if( transform.rotation.eulerAngles.z>oldEulerAngles.z)
		   {
			   steerInput=1;
		   }
		   else{
			   steerInput=-1;
		   }
		              oldEulerAngles = transform.rotation.eulerAngles;
      }
	  
		SteerDirection = SmoothSteering (steerInput);
		SteerDirection = SpeedAdjustedSteering (SteerDirection);
		// Calculate the current angle the tires are pointing
		SteerAngle = SteerDirection * MaxSteerAngle;
		// Set front axle tires rotation
		AxleFront.TireRight.transform.localRotation = Quaternion.Euler(0, 0, Mathf.Rad2Deg * SteerAngle);
		AxleFront.TireLeft.transform.localRotation = Quaternion.Euler(0, 0, Mathf.Rad2Deg * SteerAngle);  
	}
	
	void FixedUpdate() {
		// Update from rigidbody to retain collision responses
		Velocity = Rigidbody2D.velocity;
		HeadingAngle = (Rigidbody2D.rotation + 90) * Mathf.Deg2Rad;

		float sin = Mathf.Sin(HeadingAngle);
		float cos = Mathf.Cos(HeadingAngle);

		// Get local velocity
		LocalVelocity.x = cos * Velocity.x + sin * Velocity.y;
		LocalVelocity.y = cos * Velocity.y - sin * Velocity.x;

		// Weight transfer
		float transferX = WeightTransfer * LocalAcceleration.x * CGHeight / WheelBase;
		float transferY = WeightTransfer * LocalAcceleration.y * CGHeight / TrackWidth * 20;		//exagerate the weight transfer on the y-axis

		// Weight on each axle
		float weightFront = Rigidbody2D.mass * (AxleFront.WeightRatio * -Physics2D.gravity.y - transferX);
		float weightRear = Rigidbody2D.mass * (AxleRear.WeightRatio * -Physics2D.gravity.y + transferX);

		// Weight on each tire
		AxleFront.TireLeft.ActiveWeight = weightFront - transferY;
		AxleFront.TireRight.ActiveWeight = weightFront + transferY;
		AxleRear.TireLeft.ActiveWeight = weightRear - transferY;
		AxleRear.TireRight.ActiveWeight = weightRear + transferY;
			
		// Velocity of each tire
		AxleFront.TireLeft.AngularVelocity = AxleFront.DistanceToCG * AngularVelocity;
		AxleFront.TireRight.AngularVelocity = AxleFront.DistanceToCG * AngularVelocity;
		AxleRear.TireLeft.AngularVelocity = -AxleRear.DistanceToCG * AngularVelocity;
		AxleRear.TireRight.AngularVelocity = -AxleRear.DistanceToCG *  AngularVelocity;

		// Slip angle
		AxleFront.SlipAngle = Mathf.Atan2(LocalVelocity.y + AxleFront.AngularVelocity, Mathf.Abs(LocalVelocity.x)) - Mathf.Sign(LocalVelocity.x) * SteerAngle;
		AxleRear.SlipAngle = Mathf.Atan2(LocalVelocity.y + AxleRear.AngularVelocity,  Mathf.Abs(LocalVelocity.x));

		// Brake and Throttle power
		float activeBrake = Mathf.Min(Brake * BrakePower + EBrake * EBrakePower, BrakePower);
		float activeThrottle = (Throttle * Engine.GetTorque (Rigidbody2D)) * (Engine.GearRatio * Engine.EffectiveGearRatio);

		// Torque of each tire (rear wheel drive)
		AxleRear.TireLeft.Torque = activeThrottle / AxleRear.TireLeft.Radius;
		AxleRear.TireRight.Torque = activeThrottle / AxleRear.TireRight.Radius;

		// Grip and Friction of each tire
		AxleFront.TireLeft.Grip = TotalTireGripFront * (1.0f - EBrake * (1.0f - EBrakeGripRatioFront));
		AxleFront.TireRight.Grip = TotalTireGripFront * (1.0f - EBrake * (1.0f - EBrakeGripRatioFront));
		AxleRear.TireLeft.Grip = TotalTireGripRear * (1.0f - EBrake * (1.0f - EBrakeGripRatioRear));
		AxleRear.TireRight.Grip = TotalTireGripRear * (1.0f - EBrake * (1.0f - EBrakeGripRatioRear));

		AxleFront.TireLeft.FrictionForce = Mathf.Clamp(-CornerStiffnessFront * AxleFront.SlipAngle, -AxleFront.TireLeft.Grip, AxleFront.TireLeft.Grip) * AxleFront.TireLeft.ActiveWeight;
		AxleFront.TireRight.FrictionForce = Mathf.Clamp(-CornerStiffnessFront * AxleFront.SlipAngle, -AxleFront.TireRight.Grip, AxleFront.TireRight.Grip) * AxleFront.TireRight.ActiveWeight;
		AxleRear.TireLeft.FrictionForce = Mathf.Clamp(-CornerStiffnessRear * AxleRear.SlipAngle, -AxleRear.TireLeft.Grip, AxleRear.TireLeft.Grip) * AxleRear.TireLeft.ActiveWeight;
		AxleRear.TireRight.FrictionForce = Mathf.Clamp(-CornerStiffnessRear * AxleRear.SlipAngle, -AxleRear.TireRight.Grip, AxleRear.TireRight.Grip) * AxleRear.TireRight.ActiveWeight;

	 	// Forces
		float tractionForceX = AxleRear.Torque - activeBrake * Mathf.Sign(LocalVelocity.x);
		float tractionForceY = 0;

		float dragForceX = -RollingResistance * LocalVelocity.x - AirResistance * LocalVelocity.x * Mathf.Abs(LocalVelocity.x);
		float dragForceY = -RollingResistance * LocalVelocity.y - AirResistance * LocalVelocity.y * Mathf.Abs(LocalVelocity.y);

		float totalForceX = dragForceX + tractionForceX;
		float totalForceY = dragForceY + tractionForceY + Mathf.Cos (SteerAngle) * AxleFront.FrictionForce + AxleRear.FrictionForce;

		//adjust Y force so it levels out the car heading at high speeds
		if (AbsoluteVelocity > 10) {
			totalForceY *= (AbsoluteVelocity + 1) / (21f - SpeedTurningStability);
			engineSound.pitch = (AbsoluteVelocity + 1) / (21f - SpeedTurningStability);
		}

		// If we are not pressing gas, add artificial drag - helps with simulation stability
		if (Throttle == 0) {
			Velocity = Vector2.Lerp (Velocity, Vector2.zero, 0.005f);
		}
	
		// Acceleration
		LocalAcceleration.x = totalForceX / Rigidbody2D.mass;
		LocalAcceleration.y = totalForceY / Rigidbody2D.mass;

		Acceleration.x = cos * LocalAcceleration.x - sin * LocalAcceleration.y;
		Acceleration.y = sin * LocalAcceleration.x + cos * LocalAcceleration.y;

		// Velocity and speed
		Velocity.x += Acceleration.x * Time.deltaTime;
		Velocity.y += Acceleration.y * Time.deltaTime;

		AbsoluteVelocity = Velocity.magnitude;

		// Angular torque of car
		float angularTorque = (AxleFront.FrictionForce * AxleFront.DistanceToCG) - (AxleRear.FrictionForce * AxleRear.DistanceToCG);

		// Car will drift away at low speeds
		if (AbsoluteVelocity < 0.5f && activeThrottle == 0)
		{
			LocalAcceleration = Vector2.zero;
			AbsoluteVelocity = 0;
			Velocity = Vector2.zero;
			angularTorque = 0;
			AngularVelocity = 0;
			Acceleration = Vector2.zero;
			Rigidbody2D.angularVelocity = 0;
		}

		var angularAcceleration = angularTorque / Inertia;

		// Update 
		AngularVelocity += angularAcceleration * Time.deltaTime;

		// Simulation likes to calculate high angular velocity at very low speeds - adjust for this
		if (AbsoluteVelocity < 1 && Mathf.Abs (SteerAngle) < 0.05f) {
			AngularVelocity = 0;
		} else if (SpeedKilometersPerHour < 0.75f) {
			AngularVelocity = 0;
		}

		HeadingAngle += AngularVelocity * Time.deltaTime;
		Rigidbody2D.velocity = Velocity;
		//Rigidbody2D.MoveRotation (Mathf.Rad2Deg * HeadingAngle - 90);

	}

	float SmoothSteering(float steerInput) {
		float steer = 0;
		if(Mathf.Abs(steerInput) > 0.001f) {
			steer = Mathf.Clamp(SteerDirection + steerInput * Time.deltaTime * SteerSpeed, -1.0f, 1.0f); 
		}
		else
		{
			if (SteerDirection > 0) {
				steer = Mathf.Max(SteerDirection - Time.deltaTime * SteerAdjustSpeed, 0);
			}
			else if (SteerDirection < 0) {
				steer = Mathf.Min(SteerDirection + Time.deltaTime * SteerAdjustSpeed, 0);
			}
		}

		return steer;
	}

	float SpeedAdjustedSteering(float steerInput) {
		float activeVelocity = Mathf.Min(AbsoluteVelocity, 250.0f);
		float steer = steerInput * (1.0f - (AbsoluteVelocity / SpeedSteerCorrection));
		return steer;
	}


		void OnTriggerEnter2D(Collider2D col)
    {
		if(col.tag == "boost")
		{
			StartCoroutine( SpeedBoost() );
			col.gameObject.active = false;
		}
        else if(col.tag == "unboost")
		{
			StartCoroutine( SpeedBad() );
			col.gameObject.active = false;
		}
		
    }

	IEnumerator SpeedBoost()
	{
		Rigidbody2D.mass =150;
		Debug.Log(Rigidbody2D.mass);
		trailLeft.SetActive(true);
		trailRight.SetActive(true);
		yield return new WaitForSeconds (2.0f);
		trailLeft.SetActive(false);
		trailRight.SetActive(false);
		Rigidbody2D.mass =350;
	}
	
	IEnumerator SpeedBad()
	{
		Rigidbody2D.mass =650;
		Debug.Log(Rigidbody2D.mass);
		yield return new WaitForSeconds (2.0f);
		Rigidbody2D.mass =350;
	}
	/*void OnCollisionEnter2D(Collision2D collision)
    {
		Debug.Log("colision");
         StartCoroutine( ActiveTurn() );
		 test = 1;
    }

	
IEnumerator ActiveTurn()
{
   turn = 0;
   Throttle = 0;
   steerInput=0;
   yield return new WaitForSeconds (1.0f);
   Debug.Log(steerInput);
   Throttle = 1;
   turn = 5;
   test=0;
}*/

}
