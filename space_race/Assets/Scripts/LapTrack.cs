﻿using UnityEngine;
using System.Collections;

public class LapTrack : MonoBehaviour {

	// next trigger in the lap
	public LapTrack next;
	
	// when an object enters this trigger
	void OnTriggerEnter2D(Collider2D other) {
		LapPos carLapCounter = other.gameObject.GetComponent<LapPos>();
		if (carLapCounter) {
			//Debug.Log("lap trigger " + gameObject.name);
			carLapCounter.OnLapTrigger(this);
		}
	}
}
