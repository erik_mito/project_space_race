﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUps : MonoBehaviour
{
    // Start is called before the first frame update
    int speed;
    int unspeed;

    public GameObject[] boost;
    public GameObject[] unboost;
    void Start()
    {
        speed=Random.Range(0, boost.Length);
        unspeed=Random.Range(0,  unboost.Length);
         boost[speed].active = true;
        unboost[unspeed].active = true;
    }

    // Update is called once per frame
    void Update()
    {
       
        if(!boost[speed].active)
        {
            speed=Random.Range(0, boost.Length);
             boost[speed].active = true;
        }
        if(!unboost[unspeed].active)
        {
            unspeed=Random.Range(0,  unboost.Length);
            unboost[unspeed].active = true;
        }
    }
}
