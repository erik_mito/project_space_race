﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car_Test : MonoBehaviour
{
    public float turnpower = 2;
    private Vector3 myVel;
    private Vector3 acceleration;
    private float maxSpeed = 0.002f;
    // Start is called before the first frame update

    GameObject CameraView;

	void Awake() {
		CameraView = GameObject.Find ("CameraView").gameObject;
  }
    void Start()
    {
      myVel = new Vector3(0,0,0);
      acceleration = new Vector3(0,0,0);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
     if(Input.GetKey(KeyCode.W))
     {
       float currentspeed = myVel.magnitude;
       myVel.Normalize();
       transform.Translate(0,acceleration.y,0);
       if(myVel.magnitude<=maxSpeed)
       {
         acceleration.y += 0.001f;
       }
     }


     if(Input.GetKey(KeyCode.S))
     {
      float currentspeed = myVel.magnitude;
       myVel.Normalize();
       transform.Translate(0,acceleration.y,0);
       if(acceleration.y>=-2 *maxSpeed)
       {
         acceleration.y -= 0.001f;
       }
     }
      if(Input.GetKey(KeyCode.A))
      {
      transform.Rotate(Vector3.forward * turnpower);
      }
      if(Input.GetKey(KeyCode.D))
      {
      transform.Rotate(Vector3.forward * -turnpower);
      }  
     
     transform.position +=myVel;
     gas();
     CameraView.transform.position = this.transform.position;
    }
 void gas()
     {
       if(!Input.GetKey(KeyCode.W)&&!Input.GetKey(KeyCode.S))
       {
         if(acceleration.y>= 0)
         {
           transform.Translate(0,acceleration.y,0);
           acceleration.y -= 0.001f;
         }
         else if(acceleration.y<=0)
         {
           transform.Translate(0,acceleration.y,0);
           acceleration.y += 0.001f;
         }
       }
     }
  


}
